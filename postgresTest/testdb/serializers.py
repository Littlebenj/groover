from rest_framework import serializers
from .models import Artist, Album

class AlbumSerializer(serializers.ModelSerializer):

    class Meta:
        model = Album
        fields = ('name', 'album_type','num_tracks')

class ArtistSerializer(serializers.ModelSerializer):
    
    albums = AlbumSerializer(many=True)
    class Meta:
        model = Artist
        fields= ('name', 'num_followers', 'popularity', 'albums')

# Generated by Django 3.1.2 on 2020-10-25 11:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('testdb', '0005_auto_20201025_1100'),
    ]

    operations = [
        migrations.AddField(
            model_name='artist',
            name='popularity',
            field=models.IntegerField(default=0),
        ),
    ]

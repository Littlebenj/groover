from django.test import TestCase
from .models import Artist, Album
from datetime import datetime
# Create your tests here.

class ArtistTestCase(TestCase):
    def setUp(self):
        Artist.objects.create(name="Madonna", num_followers=666, popularity=50)

    def test_artists_fields(self):
        madonna = Artist.objects.get(name="Madonna")
        self.assertEqual(madonna.num_followers, 666)
        self.assertEqual(madonna.popularity, 50)

class AlbumTestCase(TestCase):
    def setUp(self):
        m = Album.objects.create(name="Tears of the devil", release_date=datetime.now(), num_tracks=5, album_type="album")
        Album.objects.get(name="Tears of the devil").artists.add(Artist.objects.create(name="Madonna", num_followers=666, popularity=50))
    
    def test_album_fields(self):
        album = Album.objects.get(name="Tears of the devil")
        self.assertEqual(album.album_type, "album")
        self.assertEqual(album.release_date, datetime.now().date())
        
from django.db import models
from model_utils.models import TimeStampedModel
# Create your models here.

class Album(TimeStampedModel):
    id = models.CharField(max_length=80, primary_key=True) 
    album_type = models.CharField(max_length=80)
    num_tracks = models.IntegerField()
    name = models.CharField(max_length=800)


    def __str__(self):
        return self.name

class Artist(TimeStampedModel):
    id = models.CharField(max_length=80, primary_key=True) 
    name = models.CharField(max_length=800)
    num_followers = models.IntegerField(default=0)
    popularity = models.IntegerField(default=0)
    albums = models.ManyToManyField(Album)

    
    def __str__(self):
        return self.name




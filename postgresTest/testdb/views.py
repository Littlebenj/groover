from django.shortcuts import render
import requests
from .spotify_client import SpotifyAPI
from .models import Artist, Album
import json
from django.core.cache import cache
from django.core import serializers
from django.http import JsonResponse
from datetime import datetime, timezone
from .serializers import ArtistSerializer, AlbumSerializer

# Create your views here.
def api(request):
    Artist.objects.all().delete()
    Album.objects.all().delete()
    if Artist.objects.count() == 0 or datetime.now(timezone.utc).date() > Artist.objects.first().modified.date():
        spotify = SpotifyAPI()
        new_releases = spotify.new_releases()

        # deleting existing results
        Artist.objects.all().delete()
        Album.objects.all().delete()

        # fetching results from spotify api
        for album in new_releases["albums"]["items"]:
            #album_json = spotify.get_album(album["id"])
            #print(album_json)
            #album_model, created = Album.objects.update_or_create(id=album["id"], defaults={'album_type':album['album_type'], 'name':album['name'],'release_date':album['release_date'], 'num_tracks':album['total_tracks']})    
            for artist_item in album["artists"]:
                artist_json = spotify.get_artist(artist_item["id"])
                Artist.objects.update_or_create(id = artist_json['id'], defaults={'name':artist_json['name'],'num_followers':artist_json["followers"]["total"], 'popularity':artist_json["popularity"]})

        for artist in Artist.objects.all():
            album_json = spotify.get_album_by_artist(artist.id)
            for album_item in album_json["items"]:
                #print(album_item)
                album, created = Album.objects.get_or_create(id=album_item['id'], defaults={ 'name':album_item['name'], 'num_tracks':album_item['total_tracks']})
                #print(album)
                #print(artist.albums)
                #if album not in artist.albums:
                artist.albums.add(album)  
        print(spotify.get_album_by_artist('66CXWjxzNUsdJxJ2JdwvnR'))
    artist_serializer = ArtistSerializer(Artist.objects.all(), many=True)
    return JsonResponse(artist_serializer.data, safe=False)